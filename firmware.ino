#include <ESP8266WiFi.h>
#include <ESP8266HTTPClient.h>
#include <WiFiClient.h>

// Set here you the connection to the RGB Led
int red_light_pin = 16; //D0
int green_light_pin = 5; //D1
int blue_light_pin = 2; //D4

int btnPins[4];
int btnState[4];
int nBtn = 4;

// Configure here your WiFi access
const char* ssid = "<your-wifi-ssid>";
const char* password = "<your-wifi-password>";

// Set your network configuration
IPAddress local_IP(192, 168, 1, 144);
IPAddress gateway(192, 168, 1, 1);
IPAddress subnet(255, 255, 0, 0);
IPAddress primaryDNS(8, 8, 8, 8);   //optional
IPAddress secondaryDNS(8, 8, 4, 4); //optional

void setup() {
  Serial.begin(9600);

  pinMode(red_light_pin, OUTPUT);
  pinMode(green_light_pin, OUTPUT);
  pinMode(blue_light_pin, OUTPUT);

  btnPins[1] = 14; // D5
  btnPins[2] = 12; // D6
  btnPins[3] = 13; // D7
  btnPins[4] = 15; // D8

  for (byte i = 1; i <= nBtn; i = i + 1) {
    pinMode(btnPins[i], INPUT);
  }
  
  btnState[1] = 0;
  btnState[2] = 0; 
  btnState[3] = 0; 
  btnState[4] = 0;

  RGB_color(255, 0, 0);

  // Configures static IP address
  if (!WiFi.config(local_IP, gateway, subnet, primaryDNS, secondaryDNS)) {
    Serial.println("STA Failed to configure");
  } 

  delay(500);
}

void loop() {

    //Check WiFi connection status
    if(WiFi.status()== WL_CONNECTED){
      WiFiClient client;
      HTTPClient http;

      for (byte i = 1; i <= nBtn; i = i + 1) {
      btnState[i] = digitalRead(btnPins[i]);
      if (btnState[i] == HIGH) {
      Serial.println(String("Button ") + i + String(" pressed"));

      RGB_color(255, 200, 0);

      WiFiClient client;
      HTTPClient http;

      String serverPath = String("http://www.yoursite.com/link-to-button-") + i;
      
      // Your Domain name with URL path or IP address with path
      http.begin(client, serverPath.c_str());
      
      // Send HTTP GET request
      Serial.print(serverPath);
      int httpResponseCode = http.GET();
      
      if (httpResponseCode>0) {
        Serial.print("HTTP Response code: ");
        Serial.println(httpResponseCode);
        String payload = http.getString();
        Serial.println(payload);
        RGB_color(255, 200, 0);
        delay(1000);
      }
      else {
        Serial.print("Error code: ");
        Serial.println(httpResponseCode);
        RGB_color(255, 0, 0);
        delay(1000);
      }
      
      // Free resources
      http.end();
      RGB_color(0, 150, 255);
      
    } else {
      // turn LED off:
      //digitalWrite(led1Pin, LOW);
    }
  }
    
    }else{
      
      // Not connected, try to reconnect
      RGB_color(252, 119, 3);
      WiFi.begin(ssid, password);
      Serial.println("Connecting");
      while(WiFi.status() != WL_CONNECTED) {
        delay(500);
        Serial.print(".");
      }
      Serial.println("");
      Serial.print("Connected to WiFi network with IP Address: ");
      Serial.println(WiFi.localIP());
      RGB_color(0, 150, 255);
      
    }
}

void RGB_color(int red_light_value, int green_light_value, int blue_light_value)
 {
    analogWrite(red_light_pin, red_light_value);
    analogWrite(green_light_pin, green_light_value);
    analogWrite(blue_light_pin, blue_light_value);
}
