# Buttoncino

> A physical button for the web

The idea behind this project was simply: I just had the need of a button in my real world that I could press to make something happens in the digital world. In my case the aim of this application was, more concretely, the desire to invoke an Alexa skill by simply pressing a button on my desk. Surfing the web I discover [Voice Monkey](https://voicemonkey.io/), a service that allows you to configure virtual button for Alexa triggable by a simple API (with a GET request to an URL).

## System description

The system I created is composed by a WiFi ESP 8266 board with 4 buttons and an RBG LED. Pressing each of the 4 buttons makes a simple GET request to 4 different URL while the LED offers the user a visual feedback on the device status.

## Components

- Wemos D1 R2
- 4x push buttons
- RGB LED
- 3x 220Ω resistors
- 4x 10KΩ resistors

## Connections

Focusing on the components and the wiring, this system simply represent the combination of two basic Arduino implementations:

1. [Push button](https://www.arduino.cc/en/Tutorial/BuiltInExamples/Button)
2. [RGB LED control](https://create.arduino.cc/projecthub/muhammad-aqib/arduino-rgb-led-tutorial-fc003e)

I wanted to have four buttons, in order to launch four Alexa Skills, so I use the same diagram to include more buttons, using other pins and pull-down resistors.
